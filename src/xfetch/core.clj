(ns xfetch.core
  (:use [clojure.java.io :only [file as-url]]
        [me.raynes.fs :only [directory?
                             file?
                             parent
                             writeable?]])
  (:import [java.nio.channels Channels ReadableByteChannel]
           [java.net URL HttpURLConnection]
           [java.io FileOutputStream])
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.tools [logging :as log]]
            [clojure.string :as string])
  (:gen-class))

(defmacro info [& args]
  `(log/info (str ~@args)))

(defmacro error [e & args]
  `(log/error ~e (str ~@args)))

(defmacro debug [& args]
  `(log/debug (str ~@args)))

(defmacro warn-error [e & args]
  `(log/warn (str ~@args) ~e))

(defmacro warn [& args]
  `(log/warn (str ~@args)))

(defn log-capture! [& args]
  (apply log/log-capture! args))

(defn log-stream [& args]
  (apply log/log-stream args))

(defn valid-url? [url]
  (if-not (nil? url)
    (re-matches #"(https?|ftp)://(-\.)?([^\s/?\.#]+\.?)+(/[^\s]*)?$" url)))

(defn output-document-dir [path]
  (if-let [path (if (directory? path)
                  path
                  (.getAbsolutePath (parent path)))]
    path))

(defn valid-output-document? [path]
  (if-let [dir (output-document-dir path)]
    (writeable? dir)))

(def cli-options
  [
   ["-O" "--output-document FILE" "The documents will not be written to the appropriate files, but all will be concatenated together and written to file."
    :validate [#(valid-output-document? %) "FILE cannot be writeable"]]
   ["-h" "--help" "Print a help message describing all of xfetch’s command-line options."]])

(defn usage [options-summary]
  (->> ["Console downloader."
        ""
        "Usage: program-name [options] URL"
        ""
        "Options:"
        options-summary
        "Please refer to the manual page for more information."]
       (string/join \newline)))

(defn error-msg [errors]
  (error "The following errors occurred while parsing your command:\n\n"
         (string/join \newline errors)))

(defn exit [status msg]
  (info msg)
  (System/exit status))

(defn download [url params]
  (let [url (as-url url)
        rbc (Channels/newChannel (.openStream url))]
    (-> params
        :output-document
        FileOutputStream.
        .getChannel
        (.transferFrom rbc 0 Long/MAX_VALUE))))

(defn -main [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      (not= (count arguments) 1) (exit 1 (usage summary))
      (not (valid-url? (first arguments))) (exit 1 "Not a valid URL format.")
      errors (exit 1 (error-msg errors)))
    (download (first arguments) options)))
