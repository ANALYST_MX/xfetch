(defproject xfetch "0.1.0-SNAPSHOT"
  :description "Console downloader"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.cli "0.3.1"]
                 [org.clojure/tools.logging "0.3.1"]
                 [me.raynes/fs "1.4.6"]]
  :main xfetch.core)
